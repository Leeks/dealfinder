{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}
module Lib
    ( app
    ) where

import Control.Concurrent
import Data.Text (dropAround, pack, unpack)
import Data.Time
import Data.Char
import Data.Maybe
import Database.HDBC
import Database.HDBC.PostgreSQL
import Debug.Trace
import Network.HTTP.Simple
import Text.HTML.Scalpel.Core
import Text.StringLike

data PollTiming = Early | Late | OnTime

app :: String -> [String] -> IO ()
app pgConnString watchList = do
  conn <- connectPostgreSQL pgConnString
  smartPoll job
  where
    job :: IO (PollTiming)
    job = do
      response <- httpLBS "https://charlotte.craigslist.org/search/sys?hasPic=1&min_price=0&max_price=9999"
      timeZone <- getCurrentTimeZone
      let responseBody = getResponseBody response
      let firstPage = processCraigslistPage responseBody timeZone
      mapM (putStrLn . show) $ fromJust firstPage
      return OnTime

smartPoll :: IO (PollTiming) -> IO ()
smartPoll job = do
  job
  return ()

data Listing = Listing String Int String UTCTime
  deriving Show

processCraigslistPage :: forall str. (Ord str, StringLike str, Show str) => str -> TimeZone -> Maybe [Listing]
processCraigslistPage str timeZone = scrapeStringLike str listings
  where
    listings :: Scraper str [Listing]
    listings = chroot ("ul" @: [hasClass "rows"]) allListings

    allListings :: Scraper str [Listing]
    allListings = chroots ("li" @: [hasClass "result-row"]) listingWithImageAndPrice

    listingWithImageAndPrice :: Scraper str Listing
    listingWithImageAndPrice = doy
      title <- fmap toString $ text $ "a" @: [hasClass "result-title"]
      time <- fmap parseTime $ attr "datetime" $ "time" @: [hasClass "result-date"]
      location <- fmap dropParens $ text $ "span" @: [hasClass "result-hood"]
      price <- fmap toPriceInt $ text $ "span" @: [hasClass "result-price"]
      return $ Listing title price location time

      where
        parseTime :: str -> UTCTime
        parseTime str = localTimeToUTC timeZone localTime
          where
            localTime = parseTimeOrError True defaultTimeLocale "%F %R" $ toString str

        dropParens :: str -> String
        dropParens = (unpack . (dropAround (not . isAlphaNum)) . pack . toString)

        toPriceInt :: str -> Int
        toPriceInt = read . drop 1 . toString
