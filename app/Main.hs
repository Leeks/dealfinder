module Main where

import System.Environment

import Lib

main :: IO ()
main = do
  postgresConnectionString <- getEnv "DEALFINDER_CONN_STR"

  let watchList = [ "https://charlotte.craigslist.org/search/sys?hasPic=1&min_price=0&max_price=9999" -- All computers
                  ]

  app postgresConnectionString watchList
